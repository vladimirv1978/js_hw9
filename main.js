let timerSecond = 3;

const arr = ["Kharkiv","Kiev",["Borispol","Boyarka","Bucha","Brovary",["Navodnitsky", "Mezhigorsky", "Starokievskiy"],"Vasilkov","Irpin",],"Odessa","Lviv","Dnieper",];

function listOfElements(arrElement, requestByTag = "body") {
  let tagName = [...document.querySelectorAll(requestByTag)].reverse();
  let selector = tagName[0];

  for (let i = 0; i <= arrElement.length - 1; i++) {
    if (arrElement[i + 1] instanceof Object) {
      selector.insertAdjacentHTML("beforeend", `<ul>${arrElement[i]}</ul>`);
      continue;
    };
    if (arrElement[i] instanceof Object) {
      listOfElements(arrElement[i], "ul");
      continue;
    };

    selector.insertAdjacentHTML("beforeend", `<li>${arrElement[i]}</li>`);
  }
};

listOfElements(arr);

document.body.insertAdjacentHTML("beforeend",`<h1 class="timerField" style="text-align:center; color:blue; " >${timerSecond}</h1>`);

let timer = function () {
  document.querySelector(".timerField").innerHTML = timerSecond;
  timerSecond = timerSecond - 1;
  if (timerSecond == -2) {
    clearInterval(start);
    document.write("");
  }
};

let start = setInterval(timer, 1000);



//Основное (упрощенное) задание.----------------------------------------------------------
// const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

// function listOfElements(item, tegName = "body") {
//   let selector = document.querySelector(tegName);
//   console.log(selector);
//   item.map((element) =>
//     selector.insertAdjacentHTML("beforeend", `<li>${element}</li>`)
//   );
// }

// listOfElements(arr);


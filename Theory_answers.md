1.  Опишіть, як можна створити новий HTML тег на сторінці.

 - Тег можно создать с помощью document.createElement(tag): 
    
            Пример: let div = document.createElement('div');

Вставить его на страницу с помощью специальных методов вставки:

    node.append(...узлы или строки) – добавляет узлы или строки в конец node,

    node.prepend(...узлы или строки) – добавляет узлы или строки в начало node,

    node.before(...узлы или строки) – добавляет узлы или строки перед node,

    node.after(...узлы или строки) – добавляет узлы или строки после node,

    node.replaceWith(...узлы или строки) – заменяет node заданными узлами или строками.

            Пример: div.before('Привет!', document.createElement('h1')); - но 'Привет!' вставится как текст на страницу, а не как Html.

 - Также с помощью универсального метода insertAdjacentHTML.

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

    elem.insertAdjacentHTML(куда, html) 

    Первый параметр - это кодовое слово, которое указывает куда вставлять относительно elem.
    
    Его значения могут быть:

    "beforebegin" – вставить html непосредственно перед elem,

    "afterbegin" – вставить html в elem, в начале,

    "beforeend" – вставить html в elem, в конце,

    "afterend" – вставить html непосредственно после elem.
    
3. Як можна видалити елемент зі сторінки?

    Чтобы удалить узел используется метод node.remove()

    Пример: div.remove();


    
